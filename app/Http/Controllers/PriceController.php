<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PriceController extends Controller
{
    public function index() {
        
        $data = \App\price::all();

        if(count($data) > 0){
            $res['message'] = "Success!";
            $res['values'] = $data;
            return response($res);
        }
        else{
            $res['message'] = "Empty!";
            return response($res);
        }   
    }

public function show($id) {
    $data = \App\price::where('id',$id)->get();
    if(count($data) > 0){
        $res['message'] = "Success!";
        $res['values'] = $data;
        return response($res);
    }  else {
        $res['message'] = "Failed!";
        return response($res);
    }
}

}
