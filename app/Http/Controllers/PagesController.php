<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PagesController extends Controller
{
    public function index()
    {
        $pages=\App\pages::all();
        return view('pages/index',compact('pages'));    
    }

    public function create()
    {
        return view('pages/create');
    }

    public function store(Request $request)
    {
        $pages = new \App\Pages;
        $pages->page_title = $request->get('page_title');
        $pages->page_slug = str_slug($request->input('page_title'));
        $pages->page_sumary = $request->get('page_sumary');
        $pages->page_description = $request->get('page_description');
        $pages->page_status = $request->get('page_status');
        $pages->save();
        
        return redirect('pages')->with('success', 'Data pages telah ditambahkan');     
    }

    public function show($id)
    {
        $page = \App\Pages::find($id);
        return view('pages/show',compact('page','id'));     }

    public function edit($id)
    {
        $page = \App\Pages::find($id);
        return view('pages/edit',compact('page','id'));   
    }

    public function update(Request $request, $id)
    {
        $pages = \App\Pages::find($id);
        $pages->page_title = $request->get('page_title');
        $pages->page_slug = $request->get('page_slug');
        $pages->page_sumary = $request->get('page_sumary');
        $pages->page_description = $request->get('page_description');
        $pages->page_status = $request->get('page_status');
        $pages->save();
        return redirect('pages')->with('success', 'Data pages telah diubah');     
    }

    public function destroy($id)
    {
        $pages = \App\Pages::find($id);
        $pages->delete();
        return redirect('pages')->with('success','Data pages telah di hapus');
    }
}
