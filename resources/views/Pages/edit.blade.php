@extends('layouts.app')
@section('content')
<link rel="stylesheet" href="{{asset('css/app.css')}}">
<body>
		<div class="wrapper d-flex align-items-stretch">
			<nav id="sidebar">
				<div class="custom-menu">
					<button type="button" id="sidebarCollapse" class="btn btn-primary">
	          <i class="fa fa-bars"></i>
	          <span class="sr-only">Toggle Menu</span>
	        </button>
        </div>
				<div class="p-4 pt-5">
		  		<h1><a href="index.html" class="logo">Rozaka</a></h1>
          <ul class="list-unstyled components mb-5">
          <li>
                <a href="{{route('home')}}">Home</a>
	          </li>
	          <li>
                <a href="{{ action("PagesController@index")}}">Pages</a>
            </li>
            
            <li>
            <a href="{{ action("PriceController@index")}}">Price</a>
            </li>

    
	            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
	            </ul>
          </div>          
    	</nav>

      <div id="content" class="p-4 p-md-5 pt-5">
      <div class="container">
      <form method="post" action="{{action('PagesController@update', $id)}}">
        @csrf
        <input name="_method" type="hidden" value="PATCH">
        <div class="row">
          <div class="form-group col-md-8">
            <label for="Title">Title:</label>
            <input type="text" class="form-control" name="page_title" value="{{$page->page_title}}">
          </div>
        </div>
        <div class="row">
            <div class="form-group col-md-8">
              <label for="slug">Slug:</label>
              <input type="text" class="form-control" name="page_slug" value="{{$page->page_slug}}">
            </div>
        </div>

        <div class="row">
            <div class="form-group col-md-8">
              <label for="Summary">Summary:</label>
              <input type="text" class="form-control" name="page_sumary" value="{{$page->page_sumary}}">
            </div>
        </div>

        <div class="row">
            <div class="form-group col-md-8">
              <label for="Description">Description:</label>
              <textarea name="page_description" rows="4" cols="50"> {{$page->page_description}} </textarea>
            </div>
        </div>
          
        <div class="row">
            <div class="form-group col-md-4">
                <label>Status:</label>
                <select class="form-control" name="publisher">
                  <option value="1">active</option>
                  <option value="0"> No Active</option>
                </select>
            </div>
        </div>

        <div class="row">
          <div class="form-group col-md-4" style="margin-top:60px">
            <button type="submit" class="btn btn-success">Submit</button>
            <button type="submit" class="btn btn-danger">Cancel</button>

          </div>
        </div>
      </form>
    </div>
      </div>
    </div>
    <script src="https://cdn.ckeditor.com/4.13.1/standard/ckeditor.js"></script>
    <script>
                        CKEDITOR.replace('page_description');
    </script>
  </body>
@endsection